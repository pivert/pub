import threading
import requests
from collections import deque
from statistics import median_high
import datetime

backends = {
    'Akira': {
        'urls': ["https://akira",],
        'headers': {'Host': 'www.pivert.org'},
        'verify': False,
    },
    'Pivert': {
        'urls': ['https://www.pivert.org',],
    },
    'Post': {
        'urls': [
            'https://www.post.lu/particuliers/colis-courrier/track-and-trace#/search',
            'https://www.post.lu/fr/particuliers',
        ]
    }
}

class WebChecker(threading.Thread):
    def __init__(self, be_name, be_config, interval=10, max_len=10):
        threading.Thread.__init__(self)
        self.name = be_name
        self.config = be_config
        self.interval = interval
        self.results = deque(max_len * [datetime.timedelta(0)], max_len)
        self.verify = be_config['verify'] if 'verify' in be_config.keys() else True
        verify_exists = 'verify' in be_config.keys()
        self.headers = be_config['headers'] if 'headers' in be_config.keys() else None
    
    def run(self):
        # Immediately schedule next run
        threading.Timer(self.interval, self.run, )
        totalrt = datetime.timedelta(0)
        for url in self.config['urls']:
            r = requests.get(url, verify=self.verify, headers=self.headers, timeout=10)
            # r = requests.get(url, verify=self.verify, headers={'host': 'www.pivert.org'}, timeout=10)
            totalrt += r.elapsed
        self.results.appendleft(totalrt/len(self.config['urls']))

    def get_median_high(self):
        return median_high(self.results).total_seconds()

for be_name, be_config in backends.items():
    print(f'Backend name: {be_name}')
    for k,v in be_config.items():
        print(f'- {k}: {v}')
    backends[be_name]['webchecker'] = WebChecker(be_name, be_config)
    backends[be_name]['webchecker'].start()

for be_name in backends:
    backends[be_name]['webchecker'].join()
    print(backends[be_name]['webchecker'].get_median_high())

